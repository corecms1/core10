<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id('user_id');
            $table->string('user_level');
            $table->string('user_logname');
            $table->string('user_password')->nullable();
            $table->string('user_name')->nullable();
            $table->string('user_email')->unique()->nullable();

            $table->longText('user_details')->nullable();
            $table->string('user_default',10)->default('yes');
            $table->tinyInteger('user_flg')->default(1);
            $table->timestamp('user_stamp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
