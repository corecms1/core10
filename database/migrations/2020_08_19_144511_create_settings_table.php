<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id('setting_id');
            $table->string('setting_type',100)->default('global')->comment('Global|Private|Public');
            $table->string('setting_title',250);
            $table->longText('setting_value')->nullable();
            $table->string('setting_default',10)->default('system');
            $table->tinyInteger('setting_flg')->default(1);
        });

        // Insert Data to DB
        DB::table('settings')->insert([
            [
                'setting_type' => 'global',
                'setting_title' => 'site_title',
                'setting_value' => 'Core 10 New Begining',
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'global',
                'setting_title' => 'site_slogan',
                'setting_value' => 'Getting Started',
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'global',
                'setting_title' => 'mail',
                'setting_value' => '{"smtp_host":"","smtp_user":"","smtp_pass":"","smtp_port":""}',
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'global',
                'setting_title' => 'site_url',
                'setting_value' => 'http://127.0.0.1/',
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'global',
                'setting_title' => 'auth_key',
                'setting_value' => '',
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'public',
                'setting_title' => 'site_status',
                'setting_value' => 'online',
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'public',
                'setting_title' => 'site_visibility',
                'setting_value' => 'public',
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'global',
                'setting_title' => 'offline_message',
                'setting_value' => 'We will be back',
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'public',
                'setting_title' => 'page_load',
                'setting_value' => 'blog',
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'public',
                'setting_title' => 'page_main',
                'setting_value' => 'latest_post',
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'public',
                'setting_title' => 'post_show',
                'setting_value' => 'summary',
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'global',
                'setting_title' => 'post_per_page',
                'setting_value' => 10,
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'global',
                'setting_title' => 'pagination_show',
                'setting_value' => 5,
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'public',
                'setting_title' => 'seo_global',
                'setting_value' => '',
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'public',
                'setting_title' => 'seo_custom',
                'setting_value' => '',
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'global',
                'setting_title' => 'inheritance',
                'setting_value' => '',
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'global',
                'setting_title' => 'module',
                'setting_value' => '',
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'global',
                'setting_title' => 'variable',
                'setting_value' => '#\#{\[(.*?)\]\}#',
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'global',
                'setting_title' => 'api_url',
                'setting_value' => '',
                'setting_default' => 'system',
                'setting_flg' => 1
            ],
            [
                'setting_type' => 'global',
                'setting_title' => 'directory',
                'setting_value' => '{"extension":{"view":"extend/","asset":"assets/extend"},"theme":{"main":{"name":"starter","view":"themes/starter","asset":"assets/themes/starter"},"child":{"name":"","view":"","asset":""}}}',
                'setting_default' => 'system',
                'setting_flg' => 1
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
