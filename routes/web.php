<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Load Demo
Route::get('/demo', 'DemoLoad@show');
Route::get('/load', 'DemoLoad@settings');

Route::get('/login', 'DemoLoad@login');
Route::post('user/login', 'DemoLoad@valid');

Route::get('/check', 'DemoLoad@check');
