<?php

namespace App\Http\Controllers;

use App\Crud;
use App\Load;
use App\Form;
use Illuminate\Http\Request;

class DemoLoad extends Controller
{

    /* Model Instances */
    private $Crud;
    private $Load;

    /*
     *
     * To load libraries/Model/Helpers/Add custom code which will be used in this Model
     * This can ease the loading work
     *
     */
    public function __construct(){

        $this->Crud = new Crud;
        $this->Load = new Load;
    }

    //Show Page
    public function show()
    {
    	$found = $this->Crud->select_multiple('settings',array('flg' => 1),'value,type as publication,default as set');
    	dd($found);
    }

    public function settings()
    {
        $found = $this->Load->load_theme();
        dd($found);
    }

    public function login()
    {
        return view("page/login");
    }

    public function valid(Request $request)
    {
        // Validation
        $validatedData = $request->validate([
            'user_email' => 'bail|required|max:255',
            'user_password' => 'bail|required|min:3'
        ]);

        if ($validatedData == TRUE) {
            echo "Success";

            session()->put('id',1); //Create Session
            session()->put('level','admin'); //Create Session
            session()->put('logged',TRUE); //Create Session

        }else{
            return $this->login();
        }
    }

    public function check()
    {
        echo session()->get('level');
    }
}

