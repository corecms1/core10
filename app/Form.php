<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Form extends Model
{

    /*
     *
     * This is a function allowing you to pluralize and return the pluralized value
     * This function accepts one parameter
     *
     * Pass String value to be pluralized
     */
    public function pluralize($value)
    {
        //This pluralizes the string value
        $plural = Str::plural($value);
        return $plural;//return
    }

    /*
    *
    * This is a function allowing you to singularize and return the singularize value
    * This function accepts one parameter
    *
    * Pass String value to be singularize
    */
    public function singularize($value)
    {
        //This pluralizes the string value
        $singularize = Str::singular($value);
        return $singularize;//return
    }

    /*
    *
    *   GET COLUMNS NAME
    *
    *   1: Pass Table
    *   2: Pass Columns
    * 
    */
    public function column_names($table,$column)
    {
        // Singularize Table name
        $table = $this->singularize($table);

        // If not an array & string passed has no comma
        if (!is_array($column) && strpos($column,",") == False) {
            $column_name = strtolower($table.'_'.$column); //Column Name
        }else{
            // If is not an array , But string passed has a comma
            if (!is_array($column) && strpos($column,",") == True) {
                $column = explode(",", $column); /* Change String to array */
            }
            // Generate Columns Name
            for($i = 0; $i < count($column); $i++){
                $column_name[$i] = $this->column_names($table,$column[$i]); //Column Name
            }
        }
        // Return Column Names
        return $column_name;
    }












}
