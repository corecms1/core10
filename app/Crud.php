<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Crud extends Model
{


    /*
    *
    *	SET WHERE CONDITION
    *	This function is for set-up a where clause in executable format
    *
    * 	1: Pass Table Name
    * 	2: Where Clause
    * 
    */
    public function where_clause($table,$where)
    {
    	// Instance
    	$Form = new Form;
    	// Table
    	$table = strtolower($Form->pluralize($table));
        //Set key as column name and assign the value to look 
        foreach ($where as $key => $value) {
            //Set Clomun names
            $column = $Form->column_names($table,$key);
            $select_where[$column] = $value;
        }
        //Arrange Where
        foreach ($select_where as $key => $value) {
            $key_where = explode(' ', $key);
            $value_key = (array_key_exists(1, $key_where))? $key_where[1]:'=';
            $array_key = $key_where[0];

            $where_arranged[$array_key] = "$value_key,$value";
        }
        //Return The Array
        return $where_arranged;
    }

    /*
    *
    *	SELECT SINGLE VALUE
    * 	This method is for selecting single column value
    *
    * 	1: Pass Table Name
    * 	2: Pass Where clause
    * 	3: Pass Column you want to select (string)
    * 
    */
    public function select_single($table,$where,$column)
    {
    	// Instance
    	$Form = new Form;
    	// Table
    	$table = strtolower($Form->pluralize($table));
        //Columns
        $select_column = $Form->column_names($table,$column);
        // Where
        $where_column = $this->where_clause($table,$where);
        //Select Single Column Value
        $query = DB::table($table);
        foreach ($where_column as $key_where => $value_where) {
            $where_keys = explode(',', $value_where);
            $where_key = $where_keys[0];
            $value_where = $where_keys[1];
            $query = $query->where($key_where,"$where_key",$value_where);
        }
        // Limit 1
        $found = $query->limit(1)->value($select_column);
        // Return Data
        return ($found)? $found : null;
    }

    /*
    *
    *	SELECT MULTIPLE VALUE
    * 	This method is for selecting multiple column value
    *
    * 	1: Pass Table Name
    * 	2: Pass Where clause
    * 	3: Pass Columns you want to select (string or array)
    * 
    */
    public function select_multiple($table,$where,$column)
    {
    	// Instance
    	$Form = new Form;
    	// Table
    	$table = strtolower($Form->pluralize($table));
        //Columns
        $select_column = $Form->column_names($table,$column);
        // Where
        $where_column = $this->where_clause($table,$where);
        //Select Single Column Value
        $query = DB::table($table)->select($select_column);
        foreach ($where_column as $key_where => $value_where) {
            $where_keys = explode(',', $value_where);
            $where_key = $where_keys[0];
            $value_where = $where_keys[1];
            $query = $query->where($key_where,"$where_key",$value_where);
        }
        // Column Data
        $found = $query->get();
        // Return Data
        return ($found)? $found : null;
    }

    /*
    *
    *	SELECT SETTINGS
    *	This function is for selecting setting values
    *
    * 	1: Pass Title Name (as string or >=,site_title)
    * 	2: Pass Value To Select (string or array) Optional
    * 	3: Pass True to get Multiple columns (Optional)
    * 	
    */
    public function select_settings($title,$column='value',$multiple=false)
    {
    	// Check Title
    	if (!is_array($title)) {
	    	if (strpos($title,",") == True) {
	            $values = explode(",", $title); /* Change String to array */
	            $comparator = $values[0]; 
	            $item = $values[1]; 

	            $where = array("title $comparator" => $item);
	    	}else{
	            $where = array("title" => $title);
	    	}
	    }else{
            $where = $title;
	    }

		//Select Data 
		if (!$multiple) {
			$found = $this->select_single('setting',$where,$column);
		}else{
			$found = $this->select_multiple('setting',$where,$column);
		}
        // Return Data
        return ($found)? $found : null;
    }









}
