<?php

namespace App;

use App\Crud;
use Illuminate\Database\Eloquent\Model;

class Load extends Model
{
    /*
	*	Load All Default Settings
    *	
    * 	This function will load all default system settings
    */
    public function load_data()
    {
    	
    	// Load Global Settings
    	
    }


    /*
    * 	Load Global Settings
    * 	This function Load all Global Settings
    */
    public function load_global($where = array('flg' => 1))
    {

    	// Module
    	$Crud = new Crud;

    	// Where
    	$where = (!is_null($where)) ? array_merge(array('type' => 'global'),$where) : array('type' => 'global');

    	// Load Crud
    	$found = $Crud->select_multiple('settings',$where,'value,title,default,flg as status');

    	// Return
    	return $found;
    }

    /*
    * 	Load Public Settings
    * 	This function Load all Private Settings
    */
   	public function load_public($where = array('flg' => 1))
   	{

    	// Module
    	$Crud = new Crud;

    	// Where
    	$where = (!is_null($where)) ? array_merge(array('type' => 'public'),$where) : array('type' => 'public');

    	// Load Crud
    	$found = $Crud->select_multiple('settings',$where,'value,title,default,flg as status');

    	// Return
    	return $found;
   	}

    /*
    * 	Load Private Settings
    * 	This function Load all Private Settings
    */
   	public function load_private($where = array('flg' => 1))
   	{

   		// Check Logged
   		if ($this->logged()) {
	    	// Module
	    	$Crud = new Crud;

	    	// Where
	    	$where = (!is_null($where)) ? array_merge(array('type' => 'private'),$where) : array('type' => 'private');

	    	// Load Crud
	    	$found = $Crud->select_multiple('settings',$where,'value,title,default,flg as status');

	    	// Return
	    	return $found;
	    }
   	}

    /*
    * 	Load Private Settings
    * 	This function Load all Private Settings
    */
   	public function load_setting($where = null)
   	{

    	// Module
    	$Crud = new Crud;

    	// Load Crud
    	if (is_array($where) && !is_null($where)) {
	    	$found = $Crud->select_multiple('settings',$where,'value,title,default,flg as status');
	    	// Return
	    	return $found;
    	}
   	}

   	/*
   	*	Load Theme Data
   	* 	1:Pass What to find - view,name,asset
   	* 	2:Pass Theme Name 
   	*/
   	public function load_theme($get=null,$theme='main')
   	{
   		// Directory
   		$theme_found = $this->load_extend($theme,'theme');

		// Check Theme
		if (!is_null($get)) {
			$get = strtolower(trim($get));
			if (array_key_exists($get, $theme_found)) {
				$value = $theme_found[$get]; //Found get
				return $value;
			}
		}else{
			// Return All Theme Values
			return $theme_found;
		}
   	}

   	/*
   	*	Load Directory Data
   	* 	1:Pass What to find - view,asset
   	* 	2:Pass Directory Name 
   	*/
   	public function load_extend($get=null,$dir='extension')
   	{
   		// Directory
   		$directory = $this->load_setting(array('title' => 'directory'));
   		$directory = $directory[0]->setting_value;
   		$directories = json_decode($directory, True);

   		// Find
		$dir = strtolower(trim($dir));
   		$found = $directories[$dir];

		// Check Directory
		if (!is_null($get)) {
			$get = strtolower(trim($get));
			if (array_key_exists($get, $found)) {
				$value = $found[$get]; //Found get
				return $value;
			}
		}else{
			// Return All Directory Values
			return $found;
		}
   	}


   	/*
   	*
   	*	CHECK IF USER HAS LOGGED IN
   	* 
   	*/
   	public function logged()
   	{
   		// Session name
   		$session = 'logged';
   		if (session()->get($session) === TRUE) {
   			return True; //Logged In
   		}else{
   			return False; //Not Logged
   		}
   	}
}
