<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
	
	<style type="text/css">
		input.is-invalid {
			border-color: #dc3545 !important;
		}
	</style>

    <title>Login Page</title>
  </head>
  <body>
    <h1>Test Login</h1>

    <div class="container">
    	<div class="row">
    		<div class="col-md-6">

				<form method="POST" action="/user/login" autocomplete="off">@csrf
				  <div class="form-group">
				    <label>Email address</label>
				    <input type="email" name="user_email" class="form-control @error('user_email') is-invalid @enderror">
				    @error('user_email')
				    <small class="alert alert-danger">{{ $message }}</small>
					@enderror
				  </div>
				  <div class="form-group">
				    <label>Password</label>
				    <input type="password" name="user_password" class="form-control @error('user_password') is-invalid @enderror">
				  </div>
				  <div class="form-group form-check">
				    <input type="checkbox" name="remeber" class="form-check-input" id="exampleCheck1">
				    <label class="form-check-label" for="exampleCheck1">Check me out</label>
				  </div>
				  <button type="submit" class="btn btn-primary">Submit</button>
				</form>

    		</div>
    	</div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  </body>
</html>